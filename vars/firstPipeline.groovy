def call(Map pipelineParams) {
    pipeline{
    agent any
    parameters {
      string(name: 'branch',defaultValue: 'develop', description: 'Which branch/tag we may build')
    }
    triggers { cron('16 15 * * *') }

    stages{
        stage('Demo'){
            when  {
            expression { branch == 'develop' }
            }
            steps{
                sh "echo ${branch}"
                println(branch)
            }
        }
        stage('Build'){
            steps{
                sh "echo ${pipelineParams.name}"

            }
        }
        stage('Echo parameters'){
            steps{
                println(trueORfalse)
                println(alege)
                sh "date"

            }
        }
    }
    }
}
